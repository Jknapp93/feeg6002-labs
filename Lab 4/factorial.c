#include <stdio.h>
#include <limits.h>
#include <math.h>

long maxlong(void) {
	long max = LONG_MAX;
	
	return max;
}

double upper_bound(long n) {
	double m = 1;
	double p = 0;
	int i = 0;
	
	if (n == 1) {
		m = 1;
		return m;
	} 	  
	
	else if (n <= 6) {
		for(i=1; i<=n; i++) {
			m = m*i;
		}
		return m;	   
	}
	
	else {
		p = pow((n/2.0), (double)n);
		return p;
	}
}


long factorial(long n) {
	int i = 0;
	long fact = 1;
	
	if (n < 0) {
		return -2;
	}
	
	else if (n < 20) {
		for (i=1; i<=n; i++) {
			fact = fact * i;
		}
		return fact;
	}
	
	else {
		return -1;
	}
}


int main(void) {
    long i;

    /* The next line should compile once "maxlong" is defined. */
    printf("maxlong()=%ld\n", maxlong());

    /* The next code block should compile once "upper_bound" is defined. */

    for (i=0; i<50; i++) {
        printf("factorial(%ld)=%ld\n", i, factorial(i));
    }
    
    return 0;
}

